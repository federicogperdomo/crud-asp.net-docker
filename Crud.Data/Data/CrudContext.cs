﻿using Microsoft.EntityFrameworkCore;
using Crud.Neg.Models;
using System.Linq;

namespace Crud.Data.DAL
{
    public class CrudContext : DbContext
    {
        public CrudContext(DbContextOptions<CrudContext> options) : base(options)

        {
        }
        public DbSet<Employee> Employees { get; set; }

    }


    public static class DbInitializer
    {
        public static void Initialize(CrudContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Employees.Any())
            {
                return;   // DB has been seeded
            }

        }
    }
}