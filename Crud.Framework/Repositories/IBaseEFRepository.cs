﻿using Microsoft.EntityFrameworkCore;
using Crud.Franework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crud.Framework.Repositories
{
    public interface IBaseEFRepository<T> where T : EntityBase
    {
        IEnumerable<T> GetAll();
        T GetByID(int id);
        void Insert(T T); void DeleteT(int id);
        void Update(T T);
        void Save();
        void SaveChangesAsync();
    }
}
