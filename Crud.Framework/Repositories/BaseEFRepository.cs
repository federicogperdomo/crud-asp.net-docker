﻿using Microsoft.EntityFrameworkCore;
using Crud.Franework.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Crud.Framework.Repositories
{
    public class BaseEFRepository<T> : IBaseEFRepository<T> where T : EntityBase
    {
        protected DbContext _context;
        public BaseEFRepository(DbContext context)
        {
            _context = context;
        }

        public IEnumerable<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public T GetByID(int id)
        {
            return _context.Set<T>().Find(id);
        }

        public void Insert(T T)
        {
            _context.Set<T>().Add(T);
        }

        public void DeleteT(int id)
        {
            T T = _context.Set<T>().Find(id);
            _context.Set<T>().Remove(T);
        }

        public void Update(T T)
        {
            _context.Entry(T).State = EntityState.Modified;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public async void SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
