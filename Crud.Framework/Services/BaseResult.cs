﻿namespace Crud.Framework.Services
{
    public class BaseResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}