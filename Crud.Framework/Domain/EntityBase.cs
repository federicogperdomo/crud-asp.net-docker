﻿using System;

namespace Crud.Franework.Domain
{
    public class EntityBase
    {
        public int Id { get; set; }

        public bool HasId()
        {
            return Id != default(int);
        }
    }
}
