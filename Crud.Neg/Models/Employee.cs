﻿using Crud.Franework.Domain;
using System;

namespace Crud.Neg.Models
{
    public class Employee : EntityBase
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public string Telephone { get; set; }
        public string Address { get; set; }
        public DateTime EmploymentDate { get; set; }

    }
}
