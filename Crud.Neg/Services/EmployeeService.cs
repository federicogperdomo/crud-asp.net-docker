﻿using Microsoft.EntityFrameworkCore;
using Crud.Neg.Models;
using Crud.Framework.Repositories;
using Crud.Neg.Repositories;

namespace Crud.Neg.Services
{
    public class EmployeeService : IEmployeeService
    {
        public IEmployeeEFRepository _repository { get; }
        public EmployeeService(IEmployeeEFRepository repository, DbContext context)
        {
            _repository = repository;
        }
        public SavingEmployeeResult Save(SavingEmployeeContext context)
        {
            //Validations
            SavingEmployeeResult result = new SavingEmployeeResult();
            if (context.Employee.HasId())
                _repository.Insert(context.Employee);
            else
                _repository.Update(context.Employee);

            _repository.Save();
            return result;
        }
    }
}
