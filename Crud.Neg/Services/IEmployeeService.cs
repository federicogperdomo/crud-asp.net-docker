﻿namespace Crud.Neg.Services
{
    public interface IEmployeeService
    {
         SavingEmployeeResult Save(SavingEmployeeContext context);

    }
}
