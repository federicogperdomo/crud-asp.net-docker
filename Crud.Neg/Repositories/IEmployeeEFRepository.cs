﻿
using Crud.Framework.Repositories;
using Crud.Neg.Models;

namespace Crud.Neg.Repositories
{
    public interface IEmployeeEFRepository : IBaseEFRepository<Employee>
    {
    }
}
