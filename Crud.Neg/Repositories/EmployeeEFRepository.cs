﻿using Microsoft.EntityFrameworkCore;
using Crud.Neg.Models;
using Crud.Framework.Repositories;

namespace Crud.Neg.Repositories
{
    public class EmployeeEFRepository : BaseEFRepository<Employee>, IEmployeeEFRepository
    {
        public EmployeeEFRepository(DbContext context) : base(context)
        {
            
        }          
    }
}
