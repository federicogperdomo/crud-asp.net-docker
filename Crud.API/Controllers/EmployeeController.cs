﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Crud.Data.DAL;
using Crud.Neg.Models;
using Crud.Neg.Repositories;
using Crud.Neg.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Crud.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : Controller // ControllerBase
    {
        private readonly ILogger<EmployeeController> _logger;
        private readonly IEmployeeEFRepository _employeeRepository;

        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeEFRepository employeeEFRepository)
        {
            _logger = logger;
            _employeeRepository = employeeEFRepository;
        }

        [HttpGet]
        [Route("GetAll")]
        public IEnumerable<Employee> GetAll()
        {
            return _employeeRepository.GetAll();
        }

        [HttpGet]
        [Route("GetByID")]
        public Employee GetByID(int id)
        {
            return _employeeRepository.GetByID(id);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
        [Bind("Name,Type,Telephone,Address,EmploymentDate")] Employee employee)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    _employeeRepository.Insert(employee);
                    _employeeRepository.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException ex )
            {
                ModelState.AddModelError("", "Unable to save changes. ");
            }
            return View(employee);
        }

    }
}
